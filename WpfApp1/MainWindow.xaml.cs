﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            mPanel.OnMenuClick += mPanel_ButtonMenuClick;

            mPanel.OnEnumButtonClick += MPanel_OnEnumButtonClick;
        }

        private void MPanel_OnEnumButtonClick(object sender, MainPanel.MPanel.Buttons buttons)
        {
            switch (buttons)
            {
                case MainPanel.MPanel.Buttons.Preparation:
                    Console.WriteLine(1);
                    break;
                case MainPanel.MPanel.Buttons.RadioIntelligence:
                    Console.WriteLine(2);
                    break;
                case MainPanel.MPanel.Buttons.RadioSuppression:
                    Console.WriteLine(3);
                    break;
                default:
                    break;
            }
        }

        int count = 0;
        private void mPanel_ButtonMenuClick(object sender)
        {
            //Console.WriteLine(0);
            if (count > 9) count = 0;
            mPanel.ViewMode = count;
            count++;
        }

        private void one_Click(object sender, RoutedEventArgs e)
        {
            mPanel.MenuToolTip = "jkjkj";
            mPanel.ConText = "Context";
        }

        private void two_Click(object sender, RoutedEventArgs e)
        {
            mPanel.PrepText = "1";
            //mPanel.ButtonPrepText = mPanel.ButtonPrepText + mPanel.ButtonPrepText;
        }

        private void three_Click(object sender, RoutedEventArgs e)
        {
            mPanel.RIText = "2";
            //mPanel.ButtonRIText = mPanel.ButtonRIText + mPanel.ButtonRIText;
        }

        private void four_Click(object sender, RoutedEventArgs e)
        {
            mPanel.RSText = "3";
            //mPanel.ButtonRSText = mPanel.ButtonRSText + mPanel.ButtonRSText;
        }

        private void five_Click(object sender, RoutedEventArgs e)
        {
            mPanel.SRIText = "1";
            // mPanel.LabelSRIText = mPanel.LabelSRIText + mPanel.LabelSRIText;
        }

        private void six_Click(object sender, RoutedEventArgs e)
        {
            mPanel.AccessAWP = MainPanel.MPanel.AccessType.Commandor;
            // mPanel.LabelAdressText = mPanel.LabelAdressText + mPanel.LabelAdressText;
        }

        private void seven_Click(object sender, RoutedEventArgs e)
        {
            //mPanel.RoleText = "3";
            // mPanel.LabelRoleText = mPanel.LabelRoleText + mPanel.LabelRoleText;
        }

        private void eight_Click(object sender, RoutedEventArgs e)
        {
            //mPanel.AWPText = "4";
            mPanel.AWPNumber = 4;
            //mPanel.LabelAWPText = mPanel.LabelAWPText + mPanel.LabelAWPText;
        }

        private void nine_Click(object sender, RoutedEventArgs e)
        {
            mPanel.ButtonsEnabled(false, false, false);
        }

        private void ten_Click(object sender, RoutedEventArgs e)
        {
            mPanel.ButtonsEnabled(true, false, false);
        }

        private void eleven_Click(object sender, RoutedEventArgs e)
        {
            mPanel.ButtonsEnabled(false, true, false);
        }

        private void twelve_Click(object sender, RoutedEventArgs e)
        {
            mPanel.ButtonsEnabled(false, false, true);
        }

        private void thirteen_Click(object sender, RoutedEventArgs e)
        {
            mPanel.ButtonsEnabled(true, true, true);
        }

        private void fourteen_Click(object sender, RoutedEventArgs e)
        {
            mPanel.Highlight = MainPanel.MPanel.Buttons.RadioIntelligence;
            //mPanel.Operator = "Operator";
        }

        private void fifthteen_Click(object sender, RoutedEventArgs e)
        {
            mPanel.Highlight = MainPanel.MPanel.Buttons.Preparation;
        }

        private void sixteen_Click(object sender, RoutedEventArgs e)
        {
            mPanel.SetLanguage("rus");
        }

        private void seventeen_Click(object sender, RoutedEventArgs e)
        {
            mPanel.SetLanguage("eng");
        }

        private void eigthteen_Click(object sender, RoutedEventArgs e)
        {
            mPanel.RoleStation = MainPanel.MPanel.StationRole.Master;
        }

        private void nineteen_Click(object sender, RoutedEventArgs e)
        {
            mPanel.SetLanguage("az");
        }
    }
}

