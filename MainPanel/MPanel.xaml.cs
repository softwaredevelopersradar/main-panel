﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace MainPanel
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class MPanel : UserControl
    {
        public MPanel()
        {
            InitializeComponent();
            Highlight = Buttons.Preparation;
            AccessAWP = AccessType.Operator;

            ChangeView();
            RadarVisible = false;

            InitStartUp();
        }

        private void InitStartUp()
        {
            RoleStation = StationRole.Autonomic;
            SRINumber = 0;
            AWPNumber = 1;
        }

        private bool _RadarVisible = true;
        public bool RadarVisible
        {
            get { return _RadarVisible; }
            set
            {
                if (RadarVisible != value)
                {
                    _RadarVisible = value;
                    if (value == true)
                    {
                        Radar.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        Radar.Visibility = Visibility.Collapsed;
                    }
                }
            }
        }

        private int _ViewMode = 9;
        public int ViewMode
        {
            get { return _ViewMode; }
            set
            {
                if (_ViewMode != value)
                _ViewMode = value;
                ChangeView();
            }
        }

        private void ChangeView()
        {
            switch (_ViewMode)
            { 
                case 0:
                    RowGridLength(0, 40);
                    break;
                case 1:
                    RowGridLength(1, 41);
                    break;
                case 2:
                    RowGridLength(2, 40);
                    break;
                case 3:
                    RowGridLength(3, 41);
                    break;
                case 4:
                    RowGridLength(4, 40);
                    break;
                case 5:
                    RowGridLength(5, 40);
                    break;
                case 6:
                    RowGridLength(6, 40);
                    break;
                case 7:
                    RowGridLength(7, 40);
                    break;
                case 8:
                    RowGridLength(8, 40);
                    break;
                case 9:
                    RowGridLength(9, 40);
                    break;
                case 10:
                    RowGridLength(10, 40);
                    break;
                default:
                    RowGridLength(0, 40);
                    break;
            }
        }

        private void RowGridLength(int index, int gridLength)
        {
            for (int i = 0; i < myGrid.RowDefinitions.Count; i++)
            {
                if (i == index)
                {
                    myGrid.RowDefinitions[i].Height = new GridLength(gridLength);
                }
                else
                {
                    myGrid.RowDefinitions[i].Height = new GridLength(0);
                }
            }
        }

        public string MenuToolTip
        {
            get { return bMenu.ToolTip.ToString(); }
            set
            {
                bMenu.ToolTip = value;
                bMenu9.ToolTip = value;
            }
        }

        public string PrepText
        {
            get { return L1.Content.ToString(); }
            set
            {
                L1.Content = value;
                L19.Content = value;
                L110.Content = value;
            }
        }
        public string RIText
        {
            get { return L2.Content.ToString(); }
            set
            {
                L2.Content = value;
                L29.Content = value;
                L210.Content = value;
            }
        }
        public string RSText
        {
            get { return L3.Content.ToString(); }
            set
            {
                L3.Content = value;
                L39.Content = value;
                L310.Content = value;
            }
        }

        private string _ConText = "Обстановка";
        public string ConText
        {
            get { return _ConText; }
            set
            {
                _ConText = value;
                ConTextLabel = ConTextLabel;
            }
        }

        private string _ConTextLabel = "";
        public string ConTextLabel
        {
            get { return _ConTextLabel; }
            set
            {
                if (value == "")
                    l09.Content = _ConText;
                else
                    l09.Content = _ConText + " " + value;
            }
        }

        private string _SRIText = "СП";
        public string SRIText
        {
            get { return _SRIText; }
            set
            {
                _SRIText = value;
                SRINumber = SRINumber;
            }
        }

        private int _SRINumber = 0;
        public int SRINumber
        {
            get { return _SRINumber; }
            set
            {
                if (value <=0)
                {
                    l1.Content = _SRIText;
                    l19.Content = _SRIText;
                    l110.Content = _SRIText;
                }
                else
                {
                    l1.Content = _SRIText + " " + value;
                    l19.Content = _SRIText + " " + value;
                    l110.Content = _SRIText + " " + value;
                }
            }
        }

        public enum AccessType
        {
            Admin,
            Commandor,
            Operator
        }

        private AccessType _AccessAWP;
        public AccessType AccessAWP
        {
            private get { return _AccessAWP; }
            set
            {
                _AccessAWP = value;
                UpdateAcсessAWPText(value);
            }
        }

        private void UpdateAcсessAWPText(AccessType accessType)
        {
            switch (accessType)
            {
                case AccessType.Admin:
                    l2.Content = _Admin;
                    l27.Content = _Admin;
                    l28.Content = _Admin;
                    l29.Content = _Admin;
                    l210.Content = _Admin;
                    break;
                case AccessType.Commandor:
                    l2.Content = _Commandor;
                    l27.Content = _Commandor;
                    l28.Content = _Commandor;
                    l29.Content = _Commandor;
                    l210.Content = _Commandor;
                    break;
                case AccessType.Operator:
                    l2.Content = _Operator;
                    l27.Content = _Operator;
                    l28.Content = _Operator;
                    l29.Content = _Operator;
                    l210.Content = _Operator;
                    break;
            }
        }

        public enum StationRole
        {
            Autonomic,
            Master,
            Slave
        }

        private StationRole _RoleStation = StationRole.Autonomic;
        public StationRole RoleStation
        {
            private get { return _RoleStation; }
            set
            {
                _RoleStation = value;
                UpdateRoleStationText(value);
            }
        }

        private void UpdateRoleStationText(StationRole stationRole)
        {
            switch (stationRole)
            {
                case StationRole.Autonomic:
                    l39.Content = Autonomic;
                    break;
                case StationRole.Master:
                    l39.Content = Master;
                    break;
                case StationRole.Slave:
                    l39.Content = Slave;
                    break;
            }
        }

        private string _Autonomic = "Автономная";
        public string Autonomic
        {
            get { return _Autonomic; }
            set
            {
                _Autonomic = value;
                UpdateRoleStationText(_RoleStation);
            }
        }
        private string _Master = "Ведущая";
        public string Master
        {
            get { return _Master; }
            set
            {
                _Master = value;
                UpdateRoleStationText(_RoleStation);
            }
        }
        private string _Slave = "Ведомая";
        public string Slave
        {
            get { return _Slave; }
            set
            {
                _Slave = value;
                UpdateRoleStationText(_RoleStation);
            }
        }

        private string _AWPText = "АРМ";
        public string AWPText
        {
            get { return _AWPText; }
            set
            {
                _AWPText = value;
                AWPNumber = AWPNumber;
            }
        }

        private int _AWPNumber = 1;
        public int AWPNumber
        {
            get { return _AWPNumber; }
            set
            {
                _AWPNumber = value;
                l4.Content = _AWPText + " " + value;
                l49.Content = _AWPText + " " + value;
                l410.Content = _AWPText + " " + value;
            }
        }

        private string _Admin = "Администратор";
        public string Admin
        {
            get { return _Admin; }
            set
            {
                _Admin = value;
                UpdateAcсessAWPText(_AccessAWP);
            }
        }
        private string _Commandor = "Командир";
        public string Commandor
        {
            get { return _Commandor; }
            set
            {
                _Commandor = value;
                UpdateAcсessAWPText(_AccessAWP);
            }
        }
        private string _Operator = "Оператор";
        public string Operator
        {
            get { return _Operator; }
            set
            {
                _Operator = value;
                UpdateAcсessAWPText(_AccessAWP);
            }
        }

        public enum Buttons
        {
            Preparation = 0,
            RadioIntelligence = 1,
            RadioSuppression = 2
        }

        private Buttons _Highlight = Buttons.Preparation;
        public Buttons Highlight
        {
            get { return _Highlight; }
            set
            {
                HighlightFunc((int)_Highlight , false);
                _Highlight = value;
                HighlightFunc((int)value);
            }
        }

        public delegate void SimpleEvent (object sender);
        public delegate void EnumButtonEvent(object sender, Buttons buttons);

        public event EnumButtonEvent OnEnumButtonClick;

        public event SimpleEvent OnMenuClick;

        private void bMenu_Click(object sender, RoutedEventArgs e)
        {
            OnMenuClick?.Invoke(this);
        }

        private void bPrep_Click(object sender, RoutedEventArgs e)
        {
            OnEnumButtonClick?.Invoke(this, Buttons.Preparation);
        }

        private void bRI_Click(object sender, RoutedEventArgs e)
        {
            OnEnumButtonClick?.Invoke(this, Buttons.RadioIntelligence);
        }

        private void bRS_Click(object sender, RoutedEventArgs e)
        {
            OnEnumButtonClick?.Invoke(this, Buttons.RadioSuppression);
        }

        public void ButtonsEnabled(bool Prepflag, bool RIflag, bool RSflag)
        {
            bPrep.IsEnabled = Prepflag;
            if (Prepflag)
            {
                HighlightFunc(0, (int)_Highlight == 0);
                //HighlightFunc(0, false);
            }
            bRI.IsEnabled = RIflag;
            if (RIflag)
            {
                HighlightFunc(1, (int)_Highlight == 1);
                //HighlightFunc(1, false);
            }
            bRS.IsEnabled = RSflag;
            if (RSflag)
            {
                HighlightFunc(2, (int)_Highlight == 2);
               //HighlightFunc(2, false);
            }
        }

        private Brush GetCustomBrush(bool isActive)
        {
            return (isActive) ? (Brush)FindResource("FillPath") : (Brush)FindResource("ControlForegroundWhite");
        }

        private void HighlightFunc(int index, bool isActive = true)
        {
            switch (index)
            {
                case 0:
                    L1.Foreground = GetCustomBrush(isActive);
                    pathPrep.Fill = GetCustomBrush(isActive);

                    L19.Foreground = GetCustomBrush(isActive);
                    pathPrep9.Fill = GetCustomBrush(isActive);
                    L110.Foreground = GetCustomBrush(isActive);
                    pathPrep10.Fill = GetCustomBrush(isActive);
                    break;
                case 1:
                    L2.Foreground = GetCustomBrush(isActive);
                    pathRI.Fill = GetCustomBrush(isActive);

                    L29.Foreground = GetCustomBrush(isActive);
                    pathRI9.Fill = GetCustomBrush(isActive);
                    L210.Foreground = GetCustomBrush(isActive);
                    pathRI10.Fill = GetCustomBrush(isActive);
                    break;
                case 2:
                    L3.Foreground = GetCustomBrush(isActive);
                    pathRS.Fill = GetCustomBrush(isActive);

                    L39.Foreground = GetCustomBrush(isActive);
                    pathRS9.Fill = GetCustomBrush(isActive);
                    L310.Foreground = GetCustomBrush(isActive);
                    pathRS10.Fill = GetCustomBrush(isActive);
                    break;
                default: break;
            }
        }



        // функция смены языка
        public void SetLanguage(string lang)
        {
            XmlDocument xDoc = new XmlDocument();
            if (System.IO.File.Exists("XMLTranslation.xml"))
                xDoc.Load("XMLTranslation.xml");
            //if (System.IO.File.Exists("E:\\work\\Try_Projects\\PanoramaLibrary\\PanoramaLibrary\\bin\\Debug\\XMLTranslation.xml"))
            //    xDoc.Load("E:\\work\\Try_Projects\\PanoramaLibrary\\PanoramaLibrary\\bin\\Debug\\XMLTranslation.xml");
            else
            {
                switch (lang)
                {

                    case "rus":
                        MessageBox.Show("Файл XMLTranslation.xml не найден!", "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                    case "eng":
                        MessageBox.Show("File XMLTranslation.xml not found!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                    case "az":
                        MessageBox.Show("Fayl XMLTranslation.xml tapılmadı!", "Səhv!", MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                    case "azlat":
                        //MessageBox.Show("File XMLTranslation.xml not found!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "azkir":
                        //MessageBox.Show("File XMLTranslation.xml not found!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                }

                return;
            }

            SortedList<string, string> Dictionary = new SortedList<string, string>();

            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;

            string SectionName = "MainPanel_Translation";
            foreach (XmlNode x1Node in xRoot)
            {
                if (x1Node.Name == SectionName)
                {
                    foreach (XmlNode x2Node in x1Node.ChildNodes)
                    {
                        // получаем атрибут ID
                        if (x2Node.Attributes.Count > 0)
                        {

                            XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                            if (attr != null)
                            {
                                foreach (XmlNode childnode in x2Node.ChildNodes)
                                {
                                    // если узел - lang
                                    if (childnode.Name == lang)
                                    {
                                        if (!Dictionary.ContainsKey(attr.Value))
                                            Dictionary.Add(attr.Value, childnode.InnerText);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //rename(Dictionary);
            smartRename(Dictionary);
        }

        //переименование всех контролов при смене языка
        private void rename(SortedList<string, string> Dictionary)
        {
            if (Dictionary.ContainsKey("bMenu.ToolTip"))
                bMenu9.ToolTip = Dictionary["bMenu.ToolTip"];


            foreach (var control in DP9.Children)
            {
                if (control is Button)
                {
                    var b = (Button)control;

                    if (b.Content is Canvas)
                    {
                        var c = (Canvas)b.Content;

                        var childrens = c.Children;

                        for (int i = 0; i < childrens.Count; i++)
                        {
                            if (childrens[i] is Label)
                            {
                                var l = (Label)childrens[i];

                                if (Dictionary.ContainsKey(b.Name))
                                {
                                    l.Content = Dictionary[b.Name];
                                }
                            }
                        }
                    }
                    if (Dictionary.ContainsKey(b.Name + ".ToolTip"))
                        b.ToolTip = Dictionary[b.Name + ".ToolTip"];
                }
                if (control is Canvas)
                {

                    //foreach (Control leafcontrol in control.Children)
                    //{
                    //    if (leafcontrol is Label)
                    //    {
                    //        var l = (Label)leafcontrol;

                    //        if (Dictionary.ContainsKey(control.Name))
                    //        {
                    //            l.Content = Dictionary[control.Name];
                    //        }
                    //    }
                    //}
                }
            }

            //Console.Beep();

            //if (Dictionary.ContainsKey("wX"))
            //    waveformGraph1.XAxes[0].Caption = Dictionary["wX"];
            //if (Dictionary.ContainsKey("wY"))
            //    waveformGraph1.YAxes[0].Caption = Dictionary["wY"];

            //if (Dictionary.ContainsKey("dsX"))
            //    xyDataScatterGraph.XAxes[0].Caption = Dictionary["dsX"];
            //if (Dictionary.ContainsKey("dsY"))
            //    xyDataScatterGraph.YAxes[0].Caption = Dictionary["dsY"];

            //if (Dictionary.ContainsKey("iX"))
            //    intensityGraph1.XAxes[1].Caption = Dictionary["iX"];
            //if (Dictionary.ContainsKey("iY"))
            //    intensityGraph1.YAxes[0].Caption = Dictionary["iY"];


            //if (Dictionary.ContainsKey("freq"))
            //    freq = Dictionary["freq"];
            //if (Dictionary.ContainsKey("MHz"))
            //    MHz = Dictionary["MHz"];
            //if (Dictionary.ContainsKey("level"))
            //    level = Dictionary["level"];
            //if (Dictionary.ContainsKey("dB"))
            //    dB = Dictionary["dB"];
            //if (Dictionary.ContainsKey("GHz"))
            //    GHz = Dictionary["GHz"];
            //if (Dictionary.ContainsKey("second"))
            //    second = Dictionary["second"];

            //if (Dictionary.ContainsKey("bearing"))
            //    bearing = Dictionary["bearing"];

            //if (Dictionary.ContainsKey("Flabel"))
            //    flabel = Dictionary["Flabel"];
            //if (Dictionary.ContainsKey("Qlabel"))
            //    qlabel = Dictionary["Qlabel"];
            //if (Dictionary.ContainsKey("SDlabel"))
            //    sdlabel = Dictionary["SDlabel"];
            //if (Dictionary.ContainsKey("Plabel"))
            //    plabel = Dictionary["Plabel"];
            //if (Dictionary.ContainsKey("CofAvPh"))
            //    cofAvPh = Dictionary["CofAvPh"];
            //if (Dictionary.ContainsKey("CofAvPl"))
            //    cofAvPl = Dictionary["CofAvPl"];

            //if (Dictionary.ContainsKey("tSMI1"))
            //    tSMI1 = Dictionary["tSMI1"];

            //if (Dictionary.ContainsKey("tSMI2"))
            //    tSMI2 = Dictionary["tSMI2"];

            //if (Dictionary.ContainsKey("tSMI3"))
            //    tSMI3 = Dictionary["tSMI3"];

            //InitContextMenuStrip();

            //ReTranslatePanoramaLabels();

            //Console.Beep();
        }

        private void smartRename(SortedList<string, string> Dictionary)
        {
            if (Dictionary.ContainsKey("MenuToolTip"))
                MenuToolTip = Dictionary["MenuToolTip"];

            if (Dictionary.ContainsKey("PrepText"))
                PrepText = Dictionary["PrepText"];

            if (Dictionary.ContainsKey("RIText"))
                RIText = Dictionary["RIText"];

            if (Dictionary.ContainsKey("RSText"))
                RSText = Dictionary["RSText"];

            if (Dictionary.ContainsKey("ConText"))
                ConText = Dictionary["ConText"];

            if (Dictionary.ContainsKey("SRIText"))
                SRIText = Dictionary["SRIText"];

            if (Dictionary.ContainsKey("AWPText"))
                AWPText = Dictionary["AWPText"];

            if (Dictionary.ContainsKey("Admin"))
                Admin = Dictionary["Admin"];

            if (Dictionary.ContainsKey("Commandor"))
                Commandor = Dictionary["Commandor"];

            if (Dictionary.ContainsKey("Operator"))
                Operator = Dictionary["Operator"];

            if (Dictionary.ContainsKey("Autonomic"))
                Autonomic = Dictionary["Autonomic"];

            if (Dictionary.ContainsKey("Master"))
                Master = Dictionary["Master"];

            if (Dictionary.ContainsKey("Slave"))
                Slave = Dictionary["Slave"];
        }
    }



    class SumConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double tot = 0;
            foreach (double value in values)
                tot += value;
            //return tot.ToString();
            return tot;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class MaxConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            List<double> ld = new List<double>();
            foreach (double value in values)
                ld.Add(value);
            return ld.Max();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class PairMaxConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            List<double> ld = new List<double>();
            foreach (double value in values)
                ld.Add(value);
            List<double> ld2 = new List<double>();
            for (int i = 0; i < ld.Count; i=i+2)
            {
                ld2.Add(ld[i] + ld[i + 1]);
            }
            return ld2.Max();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class LeftConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double d1 = (double)values[0];
            double d2 = (double)values[1];
            return (d1 - d2) / 2d;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class LeftConverter3 : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double d1 = (double)values[0];
            double d2 = (double)values[1];
            double d3 = (double)values[2];
            return ((d1 - d2 + d3) / 2d);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
